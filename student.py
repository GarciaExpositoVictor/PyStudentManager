
students = []


class Student:

    school = "Harvard"

    """Añadir el self como parámetro hace que se referencie a la misma clase instanciada"""
    def __init__(self, name, student_id=112):
        self.name = name
        self.student_id = student_id
        students.append(self)

    def __str__(self):
        return self.name

    def get_name_capitalized(self):
        return self.name.capitalize()

    def get_school(self):
        return self.school


class HighSchoolStudent(Student):

    school = "Yale"

    def get_school(self):
        return "This is the high school " + self.school

    def get_name_capitalized(self):
        return super().get_name_capitalized() + "-Inherited"


james = HighSchoolStudent("James")

print(james.get_school())
