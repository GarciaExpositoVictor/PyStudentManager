from random import random

random.seed()

students = []


def get_students_title_case():
    students_title_case = []
    for student in students:
        students_title_case.append(student["name"].title())
    return students_title_case


def print_students_title_case():
    print(get_students_title_case())


def add_student(name, student_id=random.random()):
    student = {"name": name, "Id": student_id}
    students.append(student)


def save_file(student):
    try:
        file = open("students.txt", "a")
        file.write(student + "\n")
        file.close()
    except FileNotFoundError:
        print("An error has happened trying to save the file")
    except FileExistsError:
        print("An error has happened trying to save the file")


def read_file():
    try:
        file = open("students.txt", "r")
        for student in read_students(file):
            add_student(student)
        file.close()
    except FileNotFoundError:
        print("An error has happened trying to read the file")
    except FileExistsError:
        print("An error has happened trying to read the file")


def read_students(file):
    for line in file:
        yield line
    pass


read_file()
print_students_title_case()

student_name = input("Enter the student name: ")
student_id = input("Enter student ID: ")

add_student(student_name, student_id)
save_file(student_name)


